#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#include "../generic_array.h"

struct student{
	int age;
	char* surname;
	char* name;
};

static void printInt(void *elem){
	printf("%i", *(int*)elem);
}
 
static void printStud(void* elem){
	struct student x = *(struct student*)elem;
	printf("{ age = %i, surname = %s, name = %s }\n", x.age, x.surname, x.name);
}
 
int main(){
	/* Integer */
	int i;
	generic_array_t arr1;
	ga_create_array(&arr1, sizeof(int)); 
	for(i=0; i<15; i++)
		ga_add_element(&arr1, &i);
	ga_print_array(arr1, printInt);
 
	/* Struct Student */
	generic_array_t arr2;
	struct student x = {1, "mihai", "pirvu"};
	struct student y = {1, "gigel", "despot"};
	ga_create_array(&arr2, sizeof(struct student));  
	ga_add_element(&arr2, &x);  
	ga_add_element(&arr2, &y);
	ga_print_array(arr2, printStud);
 
	ga_clear_array(&arr1);
	ga_clear_array(&arr2);

	return 0;
}

