#include "../generic_list.h"

static void printInt(void *elem){
	printf("%i", *(int*)elem);
}

struct student{
	int age;
	char* surname;
	char* name;
};

static void printStud(void* elem){
	struct student x = *(struct student*)elem;
	printf("{ age = %i, surname = %s, name = %s }\n", x.age, x.surname, x.name);
}

int main(int argc, char **argv) {
	generic_list_head_t list, *new_list = NULL;
	struct student x = {1, "mihai", "pirvu"};
	struct student y = {1, "gigel", "despot"};

	gl_create_list(&list, sizeof(struct student));  
	gl_add_element(&list, &x);  
	gl_add_element(&list, &y);
	gl_print_list(list, printStud);
	gl_clear_list(&list);


	gl_create_list(&list, sizeof(int));
	int i;
	for(i=0; i<10; i++)
		gl_add_element(&list, &i);
	new_list = gl_deepcopy(list);
	gl_clear_list(&list);
	gl_print_list(list, printInt);
	gl_print_list(*new_list, printInt);
	gl_clear_list(new_list);
	free(new_list);

	return 0;
}
